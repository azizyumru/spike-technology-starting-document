# Spike Technology Starting Document
## Mail Giriş:
- **Adres:** *mail.google.com*
- **Kullanıcı adı:** *isim.soyisim@spike.istanbul*
- **Parola:** *size.iletilen.parola*

## Jira:
- Browser üzerinde spike mail adresine bağlı mail hesabına login olunur.
***Not:*** *Gizli sekmede açılmaması gerekmektedir. Tarayıcı üzerinde yeni profil eklenmesi tavsiye edilir.*
- [Spike](spiketechnology.atlassian.net) adresine giriş yapılır
- **“Continue with Google”** butonu ile otomatik olarak giriş yapılır. *(Bkz. Resim-1)*
- Giriş sağlanıp hesap oluşturulduktan sonra, çalışılacak projeye eklenilmek için murat.cezan@spike.istanbul adresine **“Jira - Proje Ekleme Talebi”** başlığı ile mail gönderilir.

![Resim-1](https://gitlab.com/spiketechnology/spike-technology-starting-document/-/raw/main/images/jira.png "JIRA")

## GitLab:
- Browser üzerinde spike mail adresine bağlı mail hesabına login olunur.
***Not:*** *Gizli sekmede açılmaması gerekmektedir. Tarayıcı üzerinde yeni profil eklenmesi tavsiye edilir.*
- [GitLab](gitlab.com)’a giriş yapılır ve **“Sign in with”** alanında **“Google” butonuna basılarak giriş yapılır.** *(Bkz. Resim-2)*
- Hesap oluşturulur ve “murat.cezan@spike.istanbul” mail adresine **“GitLab Proje Ekleme Talebi”** başlığı altinda GitLab kullanici adinizi alta ekleyerek mail gönderilir.

![Resim-2](https://gitlab.com/spiketechnology/spike-technology-starting-document/-/raw/main/images/gitlab.png "GITLAB")

## Figma:
- Browser üzerinde spike mail adresine bağlı mail hesabına login olunur.
***Not:*** *Gizli sekmede açılmaması gerekmektedir. Tarayıcı üzerinde yeni profil eklenmesi tavsiye edilir.*
- [Figma](https://www.figma.com/team_invite/redeem/SSbgKU4EjVjwl8UDTvRuYu) linkine tıklanır ve takım daveti için login sayfası görüntülenir.
- Acilan Pop-up sekmesınde **“Contınue with Google”** butonuna basılır ve google hesabı ile giriş yapılır. *(Bkz. Resim-3)*

![Resim-3](https://gitlab.com/spiketechnology/spike-technology-starting-document/-/raw/main/images/figma.png "FIGMA")
